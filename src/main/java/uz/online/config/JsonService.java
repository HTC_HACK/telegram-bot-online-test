package uz.online.config;


//Asadbek Xalimjonov 12/20/21 10:42 AM

import java.util.List;

public interface JsonService<T> {

    void writeJson(String file, List<T> list);
}
