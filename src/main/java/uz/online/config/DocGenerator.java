package uz.online.config;


//Asadbek Xalimjonov 12/20/21 8:08 PM

import uz.online.model.TestHistory;
import uz.online.model.User;

import java.io.File;

public interface DocGenerator {
    File pdfGenerator(User user);

    File answerSheet(User currentUser,TestHistory selectedTest);
}
