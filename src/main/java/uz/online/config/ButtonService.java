package uz.online.config;


//Asadbek Xalimjonov 12/21/21 8:37 AM

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import uz.online.model.User;

public interface ButtonService {

    ReplyKeyboard getreplyKeyboard(User user);
}
