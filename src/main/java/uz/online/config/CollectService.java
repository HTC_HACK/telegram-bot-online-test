package uz.online.config;


//Asadbek Xalimjonov 12/20/21 10:43 AM

public interface CollectService {

    void collectSubject();

    void collectUser();

    void collectHistory();
}
