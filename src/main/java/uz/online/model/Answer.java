package uz.online.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

import static uz.asad.util.Util.*;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 12/19/21 3:48 PM

public class Answer {

    private UUID id = UUID.randomUUID();
    private String body ;
    private  boolean isCorrect;

    public Answer(String body, boolean isCorrect) {
        this.body = body;
        this.isCorrect = isCorrect;
    }
}
