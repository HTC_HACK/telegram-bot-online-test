package uz.online.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static uz.asad.util.Util.*;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 12/19/21 3:48 PM

public class Subject {

    private UUID id = UUID.randomUUID();
    private String name;
    private double mark;
    private double testPrice;
    private List<Test> tests = new ArrayList<>();

    public Subject(String name, double mark, double testPrice, List<Test> tests) {
        this.name = name;
        this.mark = mark;
        this.testPrice = testPrice;
        this.tests = tests;
    }

    public Subject(String name) {
        this.name = name;
    }
}
